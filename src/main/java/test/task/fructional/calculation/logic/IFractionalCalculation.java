package test.task.fructional.calculation.logic;

import test.task.fructional.calculation.logic.helper.PostHelper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Created by serhii on 4/24/16.
 */
public interface IFractionalCalculation {
    Optional getCalculationCoefficient(String inputFile, int v1) throws FileNotFoundException;

    void postCalculationCoefficient(PostHelper postHelper) throws IOException;
}
