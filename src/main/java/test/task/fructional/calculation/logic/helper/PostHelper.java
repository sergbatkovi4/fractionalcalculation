package test.task.fructional.calculation.logic.helper;

/**
 * Created by serhii on 4/23/16.
 */
public class PostHelper {
    private String inputFile;
    private String outputFile;
    private int v2;
    private int v3;
    private int v4;

    public PostHelper(String inputFile, String outputFile, int v2, int v3, int v4) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        this.v2 = v2;
        this.v3 = v3;
        this.v4 = v4;
    }

    public String getInputFile() {
        return inputFile;
    }

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public int getV2() {
        return v2;
    }

    public void setV2(int v2) {
        this.v2 = v2;
    }

    public int getV3() {
        return v3;
    }

    public void setV3(int v3) {
        this.v3 = v3;
    }

    public int getV4() {
        return v4;
    }

    public void setV4(int v4) {
        this.v4 = v4;
    }

    @Override
    public String toString() {
        return "PostHelper{" +
                "inputFile='" + inputFile + '\'' +
                ", outputFile='" + outputFile + '\'' +
                ", v2=" + v2 +
                ", v3=" + v3 +
                ", v4=" + v4 +
                '}';
    }
}
