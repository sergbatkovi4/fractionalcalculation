package test.task.fructional.calculation.logic;

import org.apache.log4j.Logger;
import test.task.fructional.calculation.logic.helper.PostHelper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

/**
 * Created by serhii on 4/23/16.
 */
public class FractionalCalculationLogic implements IFractionalCalculation {
    private final Object lock = new Object();
    private static final Logger LOGGER = Logger.getLogger(FractionalCalculationLogic.class);

    @Override
    public Optional getCalculationCoefficient(String inputFile, int v1) throws FileNotFoundException {
        LOGGER.info(String.format("Executing get calculation coefficient from file: %s, with v1: %s", inputFile, v1));
        Optional value = getLocalByIndex(new FileInputStream(inputFile), v1, true);
        LOGGER.info(String.format("Result calculated coefficient: %s", value.get()));
        return value;
    }

    private Optional getLocalByIndex(InputStream inputStream, int v1, boolean isForGet) {
        Optional value = Optional.empty();
        try (Scanner scanner = new Scanner(inputStream)) {
            scanner.useDelimiter(",");
            int currentCount = 0;
            while (scanner.hasNextInt()) {
                int readValue = scanner.nextInt();
                if (currentCount == v1) {
                    if (isForGet) {
                        value = value.of(getCalculationCoefficient(readValue));
                    } else {
                        value = value.of(readValue);
                    }
                    break;
                }
                currentCount++;
            }
        }
        return value;
    }

    private int getCalculationCoefficient(int value) {
        if (value > 10) {
            return value - 10;
        }
        return value;
    }

    @Override
    public void postCalculationCoefficient(PostHelper postHelper) throws IOException {
        LOGGER.info(String.format("Executing post calculation coefficient with param: %s", postHelper));
        Optional inputValue = getLocalByIndex(new FileInputStream(postHelper.getInputFile()), postHelper.getV3(), false);
        if (inputValue.isPresent()) {
            String outputValue = String.valueOf(postCalculationCoefficient((int) inputValue.get() + postHelper.getV2()));
            synchronized (lock) {
                List<String> cache = loadToCache(postHelper.getOutputFile());
                cache.set(postHelper.getV4(), outputValue);
                LOGGER.info(String.format("Saving to file on position: %s, value: %s", postHelper.getV4(), outputValue));
                uploadToFile(cache, postHelper.getOutputFile());
            }
        }
    }

    private int postCalculationCoefficient(int value) {
        if (value < 10) {
            return value + 10;
        }
        return value;
    }

    private List<String> loadToCache(String outputFile) throws FileNotFoundException {
        List<String> cache = new ArrayList<>();
        try (Scanner scanner = new Scanner(new FileInputStream(outputFile))) {
            scanner.useDelimiter(",");
            while (scanner.hasNextInt()) {
                cache.add(scanner.next());
            }
        }
        return cache;
    }

    private void uploadToFile(List<String> cache, String outputFile) throws IOException {
        try (FileWriter writer = new FileWriter(outputFile)) {
            for (String value : cache) {
                writer.append(value);
                if (!cache.get(cache.size() - 1).equals(value)) {
                    writer.append(",");
                }
            }
            writer.flush();
        }
    }

}
