package test.task.fructional.calculation.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by serhii on 4/24/16.
 */
@XmlRootElement
public class GetFractionalCalculationResponse {
    private String value;

    public String getValue() {
        return value;
    }

    @XmlElement
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "GetFractionalCalculationResponse{" +
                "value='" + value + '\'' +
                '}';
    }
}
