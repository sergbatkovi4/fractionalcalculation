package test.task.fructional.calculation.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by serhii on 4/24/16.
 */
@XmlRootElement
public class PostFractionalCalculationResponse {
    private int result;

    public int getResult() {
        return result;
    }

    @XmlElement
    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "PostFractionalCalculationResponse{" +
                "result='" + result + '\'' +
                '}';
    }
}
