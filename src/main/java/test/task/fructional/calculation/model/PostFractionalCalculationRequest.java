package test.task.fructional.calculation.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by serhii on 4/24/16.
 */
@XmlRootElement
public class PostFractionalCalculationRequest {
    private String v2;
    private String v3;
    private String v4;

    public String getV2() {
        return v2;
    }

    @XmlElement
    public void setV2(String v2) {
        this.v2 = v2;
    }

    public String getV3() {
        return v3;
    }

    @XmlElement
    public void setV3(String v3) {
        this.v3 = v3;
    }

    public String getV4() {
        return v4;
    }

    @XmlElement
    public void setV4(String v4) {
        this.v4 = v4;
    }

    @Override
    public String toString() {
        return "PostFractionalCalculationRequest{" +
                "v2='" + v2 + '\'' +
                ", v3='" + v3 + '\'' +
                ", v4='" + v4 + '\'' +
                '}';
    }
}
