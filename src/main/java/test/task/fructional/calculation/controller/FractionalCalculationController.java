package test.task.fructional.calculation.controller;

import test.task.fructional.calculation.model.GetFractionalCalculationResponse;
import test.task.fructional.calculation.model.PostFractionalCalculationRequest;
import test.task.fructional.calculation.model.PostFractionalCalculationResponse;
import test.task.fructional.calculation.service.FractionalCalculationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by serhii on 4/24/16.
 */
@Path("fractionalcalculation")
public class FractionalCalculationController {
    private final FractionalCalculationService fractionalCalculationService = new FractionalCalculationService();

    @GET
    @Path("{v1}")
    @Produces(MediaType.APPLICATION_XML)
    public GetFractionalCalculationResponse get(@PathParam("v1") int v1) {
        return fractionalCalculationService.get(v1);
    }

    @GET
    @Produces("text/html")
    public String getHead() {
        StringBuffer builder = new StringBuffer("Available REST methods:")
                .append("<p>")
                .append("1. GET scheme:[//[user:password@]host[:port]]/FractionalCalculation/rest/fractionalcalculation/{index in f1}")
                .append("<p>")
                .append("2. POST scheme:[//[user:password@]host[:port]]/FractionalCalculation/rest/fractionalcalculation");
        return builder.toString();
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public PostFractionalCalculationResponse post(PostFractionalCalculationRequest postFractionalCalculationRequest) {
        return fractionalCalculationService.post(postFractionalCalculationRequest);
    }
}
