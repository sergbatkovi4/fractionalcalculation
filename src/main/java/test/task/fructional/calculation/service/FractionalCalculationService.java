package test.task.fructional.calculation.service;

import org.apache.log4j.Logger;
import test.task.fructional.calculation.logic.FractionalCalculationLogic;
import test.task.fructional.calculation.logic.IFractionalCalculation;
import test.task.fructional.calculation.logic.helper.PostHelper;
import test.task.fructional.calculation.model.GetFractionalCalculationResponse;
import test.task.fructional.calculation.model.PostFractionalCalculationRequest;
import test.task.fructional.calculation.model.PostFractionalCalculationResponse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by serhii on 4/24/16.
 */
public class FractionalCalculationService {
    private final IFractionalCalculation fractionalCalculation = new FractionalCalculationLogic();
    private static final Logger LOGGER = Logger.getLogger(FractionalCalculationLogic.class);

    public GetFractionalCalculationResponse get(int v1) {
        GetFractionalCalculationResponse getFractionalCalculationResponse = new GetFractionalCalculationResponse();
        try {
            Optional value = fractionalCalculation.getCalculationCoefficient(
                    getClass().getClassLoader().getResource("f2.csv").getFile(), v1);
            getFractionalCalculationResponse.setValue(value.get().toString());
        } catch (FileNotFoundException e) {
            LOGGER.error("While getting value occurred error", e);
        }
        return getFractionalCalculationResponse;

    }

    public PostFractionalCalculationResponse post(PostFractionalCalculationRequest postFractionalCalculationRequest) {
        PostFractionalCalculationResponse postFractionalCalculationResponse = new PostFractionalCalculationResponse();
        try {
            fractionalCalculation.postCalculationCoefficient(
                    new PostHelper(getClass().getClassLoader().getResource("f1.csv").getFile(),
                            getClass().getClassLoader().getResource("f2.csv").getFile(),
                            Integer.valueOf(postFractionalCalculationRequest.getV2()),
                            Integer.valueOf(postFractionalCalculationRequest.getV3()),
                            Integer.valueOf(postFractionalCalculationRequest.getV4())));
            postFractionalCalculationResponse.setResult(0);
        } catch (IOException e) {
            LOGGER.error("While posting value occurred error", e);
            postFractionalCalculationResponse.setResult(1);
        }
        return postFractionalCalculationResponse;
    }
}
