package test.task.fructional.calculation;

import org.apache.log4j.Logger;
import org.junit.Test;
import test.task.fructional.calculation.logic.FractionalCalculationLogic;
import test.task.fructional.calculation.logic.IFractionalCalculation;
import test.task.fructional.calculation.logic.helper.PostHelper;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by serhii on 4/20/16.
 */
public class FractionalCalculationTest {
    private static final Logger LOGGER = Logger.getLogger(FractionalCalculationTest.class);

    @Test
    public void testPostFractionalCalculationLogic() throws IOException {
        IFractionalCalculation fractionalCalculation = new FractionalCalculationLogic();
        fractionalCalculation.postCalculationCoefficient(
                new PostHelper(getClass().getClassLoader().getResource("f1.csv").getFile(),
                        getClass().getClassLoader().getResource("f2.csv").getFile(), 1, 3, 3));
    }

    @Test
    public void testGetFractionalCalculationLogic() throws IOException {
        IFractionalCalculation fractionalCalculation = new FractionalCalculationLogic();
        Optional value = fractionalCalculation.getCalculationCoefficient(
                getClass().getClassLoader().getResource("f2.csv").getFile(), 5);
        assertEquals(6,(int)value.get());
    }



    @Test
    public void testMultiThreadingGetCalculationCoefficient() throws InterruptedException, ExecutionException {
        ExecutorService executors = Executors.newFixedThreadPool(10);
        int[] V1Array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Set<Callable<String>> callables = new HashSet<Callable<String>>();
        IFractionalCalculation fractionalCalculation = new FractionalCalculationLogic();
        for (int i : V1Array) {
            callables.add(() -> {
                Optional value = fractionalCalculation.getCalculationCoefficient(getClass().getClassLoader().getResource("f2.csv").getFile(), i);
                return String.format("V1 = %s; value=%s", i, value.get().toString());
            });
        }
        List<Future<String>> futures = executors.invokeAll(callables);
        for (Future<String> future : futures) {
            do {
                if (future.isDone()) {
                    LOGGER.info(String.format("In %s", future.get()));
                }
            } while (!future.isDone());
        }
        executors.shutdown();
    }

    @Test
    public void testMultiThreadingPostCalculationCoefficient() throws InterruptedException {
        ExecutorService executors = Executors.newFixedThreadPool(10);
        int[] V1Array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        IFractionalCalculation fractionalCalculation = new FractionalCalculationLogic();
        Runnable task = () -> {
            try {
                LOGGER.info(String.format("Call before: %s", Thread.currentThread().getName()));
                fractionalCalculation.postCalculationCoefficient(
                        new PostHelper(getClass().getClassLoader().getResource("f1.csv").getFile(),
                                getClass().getClassLoader().getResource("f2.csv").getFile(), 1, 1, 1));
                LOGGER.info(String.format("Call after: %s", Thread.currentThread().getName()));
            } catch (IOException e) {
                LOGGER.error("Occurred error", e);
            }
        };

        for (int i = 0; i < 10; i++) {
            executors.submit(task);
        }
        executors.shutdown();
        executors.awaitTermination(5, TimeUnit.SECONDS);

    }
}


