package test.task.fructional.calculation.integration.test;


import org.apache.log4j.Logger;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.expect;
import static org.hamcrest.xml.HasXPath.hasXPath;

/**
 * Created by serhii on 4/24/16.
 */
public class FractionalCalculationControllerTest {
    private static final Logger LOGGER = Logger.getLogger(FractionalCalculationControllerTest.class);

    @Test
    public void testGetFractionalCalculatioSuccess() {
        expect()
                .statusCode(200)
                .body(hasXPath("/getFractionalCalculationResponse[value='6']"))
                 .when().get("FractionalCalculation/rest/fractionalcalculation/5");
    }
}
